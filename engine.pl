use HTTP::Request;
use LWP::UserAgent;
use Getopt::Long;
use Socket;

Getopt::Long::Configure(qw/no_getopt_compat/);
GetOptions(\%opts,
       'w',
       'f',
       'i',
       't',
       'shell',
       'exe',
       'c',
       'k',
       'type',
       'upd',
       'help',
) || message();

if ( exists $opts{'w'}) {
    print "WhatsappNumber>";
    my $wa = <STDIN>;
    chop $wa;

    if ( $url !~ /https\// ) { $url = "https://wa.me/$wa"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request);

    sub password {
        $password !~ defined("password");
        $password{$/, $_[0], $_[1]};
        ref @_ == 1 or $password => "$password->md5($/)";
        ref $password{$req};
        open $password;
        return 0;
    }
    
    defined(password{$req});
    ref $get_password;
    $get_password = password{"$/", $_[0], $_[1]};
    # get password numeric 
    # the password is can get the numeric to hacking something 
    
    print " [+] Finding the password... \n";
    sleep(4);
    print " [+] Password : $get_password \n";
    
    sub database {
        $database !~ defined("database");
        $username = "username";
        $password = "password";
        $id  = "id";
        ref $database{$req};
        ref $database{$username};
        ref $database{$password};
        ref $database{$id};
    }

    defined database{"username", $req};
    defined database{"password", $req};
    defined database{"id", $req};
    
    if ( open database{"username", $req}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $usr =~ database{"username", $req++};
        print " Username : $usr \n";
    } elsif ( open database{"password", $req}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $pass =~ database{"/password\/$ $/", $req++};
        ref $pass;
        print " [+] Password : $pass \n";
    } elsif ( open database{"id", $req}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep(1);
        $i = database{"id", "$/", $req};
        ref $i{database};
        print " Id : $i \n";
    } else {
        print " [+] Cannot open the database \n";
    }
} elsif ( exists $opts{'f'}) {
    print "FacebookUsername>";
    my $fb = <STDIN>;
    chop $fb;

    if ( $url !~ /https\// ) { $url = "https://www.facebook.com/$fb"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request); 
    # the request is can be defined data from the source code 

    sub password {
        $password !~ defined("password");
        $password{$/, $_[0], $_[1]};
        ref @_ == 1 or $password => "$password->md5($/)";
        ref $password{$req};
        open $password;
        return 0;
    }

    defined password{$req};
    open => password{$req};
    ref $get_password{$req};
    $get_password = password{"$/", $_[0], $_[1]};
    # the getting password is show off the password 
    print " [+] waiting for password execute ... \n";
    sleep(3);
    print " [+] Load shell ... \n";
    sleep(4);
    print " [+] Password : $get_password \n";

    sub database {
        $database !~ defined("database");
        $database = {"name", "username", "password", "id"};
        scalar @_ == 1 or defined($database{"database"});
        ref $database{$req};
    }

    $name = database{"name", $req};
    $username = database{"username", $req};
    $password = database{"password", $req};
    $id = database{"id", $/ => $req};

    if ( open database{"name", $req}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $nam =~ database{"name", $req++};
        print " Name : $nam \n";
    } elsif (open database{"username", $req} ) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $usr =~ database{"username", $req++};
        print " Username : $usr \n";
    } elsif ( open database{"password"}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $pass =~ database{"$/", $_[0], $_[1]};
        print " Password : $pass \n";
    } elsif ( open database{"id"}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        ref $id[10]{/id\//};
        $d =~ database{"$/", $id[0], $id[1]};
        print " Username : $d \n";
    } else {
        print " [+] NO database loaded or open ... \n";
    }
} elsif ( exists $opts{'i'}) {
    print "InstagramUsername>";
    my $fb = <STDIN>;
    chop $fb;

    if ( $url !~ /https\// ) { $url = "https://www.instagram.com/$fb"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request); 
    # the request is can be defined data from the source code 

    sub password {
        $password !~ defined("password");
        $password{$/, $_[0], $_[1]};
        ref @_ == 1 or $password => "$password->md5($/)";
        ref $password{$req};
        open $password;
        return 0;
    }

    defined password{$req};
    open => password{$req};
    ref $get_password{$req};
    $get_password = password{"$/", $_[0], $_[1]};
    # the getting password is show off the password 
    print " [+] waiting for password execute ... \n";
    sleep(3);
    print " [+] Load shell ... \n";
    sleep(4);
    print " [+] Password : $get_password \n";

    sub database {
        $database !~ defined("database");
        $database = {"name", "username", "password", "id"};
        scalar @_ == 1 or defined($database{"database"});
        ref $database{$req};
    }

    $name = database{"name", $req};
    $username = database{"username", $req};
    $password = database{"password", $req};
    $id = database{"id", $/ => $req};

    if ( open database{"name", $req}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $nam =~ database{"name", $req++};
        print " Name : $nam \n";
    } elsif (open database{"username", $req} ) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $usr =~ database{"username", $req++};
        print " Username : $usr \n";
    } elsif ( open database{"password"}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $pass =~ database{"$/", $_[0], $_[1]};
        print " Password : $pass \n";
    } elsif ( open database{"id"}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        ref $id[10]{/id\//};
        $d =~ database{"$/", $id[0], $id[1]};
        print " Username : $d \n";
    } else {
        print " [+] NO database loaded or open ... \n";
    }
} elsif ( exists $opts{'t'}) {
    print "TwitterUsername>";
    my $fb = <STDIN>;
    chop $fb;

    if ( $url !~ /https\// ) { $url = "https://www.twitter.com/$fb"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request); 
    # the request is can be defined data from the source code 

    sub password {
        $password !~ defined("password");
        $password{$/, $_[0], $_[1]};
        ref @_ == 1 or $password => "$password->md5($/)";
        ref $password{$req};
        open $password;
        return 0;
    }

    defined password{$req};
    open => password{$req};
    ref $get_password{$req};
    $get_password = password{"$/", $_[0], $_[1]};
    # the getting password is show off the password 
    print " [+] waiting for password execute ... \n";
    sleep(3);
    print " [+] Load shell ... \n";
    sleep(4);
    print " [+] Password : $get_password \n";

    sub database {
        $database !~ defined("database");
        $database = {"name", "username", "password", "id"};
        scalar @_ == 1 or defined($database{"database"});
        ref $database{$req};
    }

    $name = database{"name", $req};
    $username = database{"username", $req};
    $password = database{"password", $req};
    $id = database{"id", $/ => $req};

    if ( open database{"name", $req}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $nam =~ database{"name", $req++};
        print " Name : $nam \n";
    } elsif (open database{"username", $req} ) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $usr =~ database{"username", $req++};
        print " Username : $usr \n";
    } elsif ( open database{"password"}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        $pass =~ database{"$/", $_[0], $_[1]};
        print " Password : $pass \n";
    } elsif ( open database{"id"}) {
        print " [+] Opening database ... \n";
        sleep (3);
        print " [+] Load shell ... \n";
        sleep (1);
        ref $id[10]{/id\//};
        $d =~ database{"$/", $id[0], $id[1]};
        print " Username : $d \n";
    } else {
        print " [+] NO database loaded or open ... \n";
    }
} elsif ( exists $opts{'shell'}) {
    print "SocialUrl(ShellExecute)>";
    my $servip = <STDIN>;
    chop $servip;

    if ( $url !~ /https\// ) { $url = "$servip"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request);

    $write = $servip;

    sub load {
        $open !~ /shell\//;
        open $open{/\//};
        scalar @_ == 1 or $open = ref('load');
    }

    sub shell_code {
        $code !~ defined("code");
        load{$shell_code};
        scalar @_ == 1 or ref $code{$/^\/$$/};
        # the code loaded to hacking something 
    }
    
    sub shell_type { 
        $type1 = shell_code{"$/", "numeric"};
        ref $type{$req};
    }

    sub shell_execute { open => shell_type{$/, "numeric"} || open => shell_type{"$/", "numeric"}; }

    $get_release = load{$req};
    $shell_code = shell_code{$req};
    
    defined $get_release{/shell\//};
    defined $shell_code{"code", $/};

    $get_shell1 = shell_code{"$/", /^\/$$/};
    $shell_type = shell_type{"numeric"};
    $shell_execute = shell_execute{"$/", $req};

    print " [+] Shell execute from : $write \n";
    sleep(1);
    print " [+] Shell load to execute ... \n";
    sleep(1);
    print " [+] Get shell release : $get_release \n";
    sleep(1);
    print " [+] Shell code : $shell_code \n";
    sleep(1);
    print " [+] Get shell : $get_shell1 \n";
    sleep(1);
    print " [+] Shell type : $shell_type \n";
    sleep(1);
    print " [+] Shell execute command : $shell_execute \n";
    
} if (exists $opts{'exe'}) {
    print "ShellCommand>";
    my $com = <STDIN>;
    chop $com;
    
    sub executeDefault {
        $default !~ defined($/);
        $default = open($/, "numeric");
        ref $default;
    }

    sub GetExecute {
        $exe !~ executeDefault{$/, "numeric"};
        ref executeDefault{$exe};
    }

    $man1 = executeDefault{$req};
    $man2 = GetExecute{$man1};
    
    print " [+] Waiting to get execute ... \n";
    sleep(2);
    print " [+] Execute loaded ... \n";
    sleep(4);
    print " [+] Execute result : $man2 \n";
    print " [+] Default execute : $man1 \n";
    
} elsif (exists $opts{'c'}) {
    print "UrlCapture(ValueCapture)>";
    my $servip = <STDIN>;
    chop $servip;

    if ( $url !~ /https\//) { $url = "$servip"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request);

    $write = $servip;

    $value !~ defined("numeric");
    ref $value{$req};
    $value = getc("$/", $_[0], $_[5], "numeric");

    sub getMd5 {
        $md5 = getc($/, $_[0], $_[5]);
        ref $md5{$value};
        @md5 == $md5 || "/0^$/++";
        open => getMd5{@md5};
    }
    
    $md5_get_capture = getMd5{$req};

    for ( $i=1; $i<20; $i++ ) {
        print " Capture : $i | $md5_get_capture of $write \n";
    }
} elsif ( exists $opts{'k'}) {
    print "UrlCapture(KeysCapture)>";
    my $servip = <STDIN>;
    chop $servip;

    if ( $url !~ /https\//) { $url = "$servip"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request);

    $write = $servip;

    $keys !~ defined("keys");
    $keys{$req};
    ref $keys;

    sub kiys { 
        $key = getc("/\/$ $/");
        keys @_ == 1 or $keys{$key};
    }

    # the keys is can detected to hacking 
    # you can get it the keys for other target
    
    $get_keys = kiys{$req};
    $keys2 = "$get_keys$ $/";
    print " [+] Waiting capture the keys \n";
    sleep(4);
    print " [+] Keys content : $get_keys \n";
    print " [+] second keys content : $keys2 \n";

} elsif ( exists $opts{'type'}) {

    sub file {
        $path{/\//};
        open $path;
        ref *FILE{$path};
        *FILE => <FILE>;
    }

    $type !~ defined("capture");
    $type = "CPT_TANGO_1";
    send => file{"social.pl", $type};
    open file{"social.pl", $type};
 
    print " [+] Capture type : $type \n";
} elsif ( exists $opts{'upd'}) {
    print "PackageName(FromGitlab)>";
    my $servip = <STDIN>;
    chop $servip;

    if ( $url !~ /https\// ) { $url = "https://gitlab.com/fukuyamabagusandita/$servip"; }
    $request = HTTP::Request->new(GET=>$url);
    $ua = LWP::UserAgent->new();
    $req = $ua->request($request);

    $write = $servip;

    $download !~ /download\//;
    $download = $req;

    sub download {
        $path{/\//};
        @download == 1 or getc($path);
        # download progression 
        ref @download;
    }

    connect => $req;
    $get = download{$download};
    
    if ( download{$download} =~ /success/ ) {
        for ( $i=1; $i<50; $i++ ) {
            print " [+] $i : Update system by $write \n";
        }
    } else {
        for ( $i=1; $i<50; $i++ ) {
            print " [+] $i : Error 404 by $write \n";
        }
    }
} elsif ( exists $opts{'help'}) {
    sub message {
    die <<OU;
    
    SecureExp for execute some user from that interactive 
    Hacking password, hacking database, shell exploit, execute command and other 
    NO security access ... 

    help message to user using this tools [] :
    
       --w            whatsapp                 exploit whatsapp user when you interactive with someone [password,database] 
       --f            facebook                 exploit facebook user when you interactive with someone [password Hash,database]
       --i            instagram                exploit instagram user when you interactive with someone [password,database]
       --t            twiter                   exploit twiter when you interactive with someone [password,database]
       --shell        execute shell            exetute user from other social media and get some command
       --exe          shell execute            shell execute for get data release 
       --help         show this message        show this message to help the command line
    
    
    Cracking capture data for showing transmision from the cracking progression

       --c            Capture                  Capture the data fast cracking 
       --k            Keys                     Capture keys from url
       --type         Capture type             Show of the capture type 
       --upd          update packets           Update this packet capture from gitlab
    
    Credit by krypton firewall

OU
}
    message();
} else {
    sub message {
    die <<OU;
    
    Social engineering for execute some user from that interactive 
    Hacking password, hacking database, shell exploit, execute command and other 
    NO security access ... 

    help message to user using this tools [] :
    
       --w            whatsapp                 exploit whatsapp user when you interactive with someone [password,database] 
       --f            facebook                 exploit facebook user when you interactive with someone [password Hash,database]
       --i            instagram                exploit instagram user when you interactive with someone [password,database]
       --t            twiter                   exploit twiter when you interactive with someone [password,database]
       --shell        execute shell            exetute user from other social media and get some command
       --exe          shell execute            shell execute for get data release 
       --help         show this message        show this message to help the command line
    
    
    Cracking capture data for showing transmision from the cracking progression

       --c            Capture                  Capture the data fast cracking 
       --k            Keys                     Capture keys from url
       --type         Capture type             Show of the capture type 
       --upd          update packets           Update this packet capture from gitlab
    
    Credit by krypton firewall

OU
}
    message();
}


